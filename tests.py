import unittest
import datetime

from .db import DB


class DBTestCase(unittest.TestCase):
    row1 = {
            'SESSION-ID': 34523,
            'LOGLEVEL': 'DEBUG',
            'REQUEST-ID': '65d33',
            'MSG': 'Starting new session',
            'DATE': datetime.datetime(2012, 9, 13, 16, 4, 22),
            'BUSINESS-ID': 1329}

    row2 = {
        'SESSION-ID': 314523,
        'LOGLEVEL': 'WARN',
        'REQUEST-ID': '15d33',
        'MSG': 'Some message',
        'DATE': datetime.datetime(2012, 9, 15, 16, 4, 22),
        'BUSINESS-ID': 1324}

    def setUp(self):
        self.indexed_fields = ['DATE', 'LOGLEVEL', 'BUSINESS-ID', 'SESSION-ID']
        self.db = DB()
        self.db.addIndexedFields(self.indexed_fields)

    def testAddSeveralRows(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.row1, self.db.storage[0])
        self.assertEqual(self.row2, self.db.storage[1])
        self.assertItemsEqual(self.indexed_fields, self.db.indexes.keys())

    def testFilterByValue(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterByValue('SESSION-ID', 314523), self.row2)

        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterByValue('SESSION-ID', 314523), (self.row2, self.row2,))

    def testFilterByLogLevel(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterByLogLevel('WARN'), self.row2)

    def testFilterByBID(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterByBID(1329), self.row1)

    def testFilterBySID(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterBySID(314523), self.row2)

    def testFilterByDateRange(self):
        self.db.addRow(self.row1)
        self.db.addRow(self.row2)

        self.assertEqual(self.db.filterByDateRange(
            datetime.datetime(2012, 9, 13, 16, 4, 22),
            datetime.datetime(2012, 9, 14, 16, 4, 22)
        ), self.row1)

        self.assertItemsEqual(self.db.filterByDateRange(
            datetime.datetime(2012, 9, 13, 16, 4, 22),
            datetime.datetime(2012, 9, 19, 16, 4, 22)
        ), (self.row1, self.row2,))

if __name__ == '__main__':
    unittest.main()
