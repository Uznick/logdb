from operator import itemgetter
import itertools
from .profiler import profiler


class DB(object):
    storage = None
    indexes = None
    indexed_fields = None

    def __init__(self):
        self.storage = []
        self.fields = []
        self.indexed_fields = []
        self.indexes = {}

    def addIndexedFields(self, indexed_fields):
        """
            Method, that sets the fields, that should be indexed (included in search)
        """
        self.indexed_fields = indexed_fields

    @profiler
    def addRow(self, row):
        """
            Method, that adds the row to the database and indexes the fields
        """
        self.storage.append(row)
        self.indexRow(row, len(self.storage) - 1)

    @profiler
    def indexRow(self, row, position):
        """
            Method, that indexes the fields
        """
        for k in row.keys():
            if k in self.indexed_fields:
                self.indexes.setdefault(k, {}).setdefault(row[k], []).append(position)

    @profiler
    def filterByValue(self, field, value):
        """
            Method, that searches the index of the field for the value and returns the results.
        """
        try:
            row_nums = self.indexes[field][value]
        except KeyError:
            return None
        else:
            return itemgetter(*row_nums)(self.storage)


    @profiler
    def filterByLogLevel(self, level):
        """
            Method, that searches for the records with log level.
            level -- string

            Example:
                db.filterByLogLevel('WARN')
        """
        return self.filterByValue('LOGLEVEL', level)

    @profiler
    def filterByBID(self, bid):
        """
            Method, that searches for the records with business id
            bid -- int

            Example:
                db.filterByBID(1329)
        """
        return self.filterByValue('BUSINESS-ID', bid)

    @profiler
    def filterBySID(self, sid):
        """
            Method, that searches for the records with session id
            sid -- int

            Example:
                db.filterBySID(42111)
        """
        return self.filterByValue('SESSION-ID', sid)

    @profiler
    def filterByDateRange(self, date_from, date_to):
        """
            Method, that searches the index of the date field for the value in range and returns the results
            date_from -- datetime.datetime object
            date_to -- datetime.datetime object

            Example:
                db.filterByDateRange(datetime.datetime(2012, 7, 13, 16, 4, 50),  datetime.datetime(2012, 11, 13, 16, 4, 50))
        """
        indexes = self.indexes['DATE']
        row_nums = [indexes[date] for date in indexes.keys() if date_from <= date <= date_to]
        row_nums = list(itertools.chain.from_iterable(row_nums))
        if not len(row_nums):
            return None
        return itemgetter(*row_nums)(self.storage)
