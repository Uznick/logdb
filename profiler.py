from __future__ import division
import timeit


class Profiler(object):
    """
        Profiler Decorator class.

        Calculates the time taken for each decorated function to execute.
    """
    data = None

    def __init__(self):
        self.data = {}

    def __call__(self, func, *args, **kwargs):
        """
            The decorator implementation
        """
        def wrapper(*args, **kwargs):
            self.data.setdefault(func.__name__, {
                'timings': [],
            })
            try:
                start_time = timeit.default_timer()
                return func(*args, **kwargs)
            finally:
                end_time = timeit.default_timer()
                duration = end_time - start_time
                self.data[func.__name__]['timings'].append(duration)

        return wrapper

    def print_stats(self):
        """
            Method, that prints out a test report for the profiled functions.
            The results include:
                - the name of the function;
                - the number of times the function was executed
                - the minimum execution time
                - the maximum execution time
                - the average execution time
        """
        for func, stats in self.data.items():
            params = {
                'func': func,
                'num_samples': len(stats['timings']),
                'min': '%.3f' % min(stats['timings']),
                'max': '%.3f' % max(stats['timings']),
                'avg': '%.3f' % (sum(stats['timings'])/len(stats['timings'])),
            }

            print '''Function: %(func)s
NumSamples: %(num_samples)s
Min: %(min)s secs
Max: %(max)s secs
Average: %(avg)s secs''' % params

            print

profiler = Profiler()
