#!/usr/bin/env python

import sys
import csv
import datetime

from .profiler import profiler
from .db import DB

if __name__ == '__main__':
    log_file = sys.argv[1]

    csv_keys = ('DATE', 'TIME', 'LOGLEVEL', 'SESSION-ID', 'BUSINESS-ID', 'REQUEST-ID', 'MSG',)  # the columns of the log

    fields = ('DATE', 'LOGLEVEL', 'SESSION-ID', 'BUSINESS-ID', 'REQUEST-ID', 'MSG',)  # the result fields of the db
    indexed_fields = ('DATE', 'LOGLEVEL', 'BUSINESS-ID', 'SESSION-ID')  # the fields, that we should index for faster search

    # parse the log file

    db = DB()
    db.addIndexedFields(indexed_fields)

    reader = csv.DictReader(file(log_file), fieldnames=csv_keys, delimiter=' ', quotechar="'")
    for i, row in enumerate(reader):
        d = dict.fromkeys(fields)
        d['DATE'] = row['DATE'] + ' ' + row['TIME']
        d['DATE'] = datetime.datetime.strptime(d['DATE'], '%Y-%m-%d %H:%M:%S')
        d['SESSION-ID'] = int(row['SESSION-ID'].replace('SID:', ''))
        d['BUSINESS-ID'] = int(row['BUSINESS-ID'].replace('BID:', ''))
        d['REQUEST-ID'] = row['REQUEST-ID'].replace('RID:', '')
        for k, v in row.items():
            if d.get(k) or k == 'TIME':
                continue
            d[k] = v

        print d
        db.addRow(d)

    # examples of filter method calls

    db.filterByBID(1329)
    db.filterByLogLevel('WARN')
    db.filterBySID(42111)
    db.filterByDateRange(datetime.datetime(2012, 7, 13, 16, 4, 50),  datetime.datetime(2012, 11, 13, 16, 4, 50))

    # output of profiler stats

    profiler.print_stats()
